<?php
/**
 * Created by IntelliJ IDEA.
 * User: Vinicius Assis
 * E-mail: viniciusassisnascimento@gmail.com
 * Company: IHOS T.I.
 * Date: 10/03/2019
 * Time: 23:41
 *
 * Changelog
 * #############################
 * Date: 16/03/2019
 * Version Release: 1.0.1
 * + Added function ihoscwp7_LoaderFunction
 * #############################
 * Date: 13/03/2019
 * Version Release: 1.0.0
 * # Fix/Revised methods CreateAccount, SuspendAccount, UnsuspendAccount, TerminateAccount, ChangePassword, ChangePackage
 * + Added detailed success and error logs in all methods
 * + Added Verification on file for not directly accessing
 * + Added exception handling
 * #############################
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

/**
 * @return array
 */
function ihoscwp7_MetaData()
{
    return array(
        'DisplayName' => 'IHOS CWP7',
        'APIVersion' => '1.1', // Use API Version 1.1
        'RequiresServer' => true,
        'DefaultSSLPort' => '2304', // Default SSL Connection Port
        'ServiceSingleSignOnLabel' => 'Login to Panel as User',
        'AdminSingleSignOnLabel' => 'Login to Panel as Admin',
    );
}

/**
 * @return array
 */
function ihoscwp7_ConfigOptions()
{
    return array(
        'PACKAGE-NUMBER' => array(
            'Type' => 'text',
            'Loader' => 'ihoscwp7_LoaderFunction',
            'SimpleMode' => true,
            'Description' => 'Package ID'
        ),
        'Inode' => array(
            'Type' => 'text',
            'Default' => '0',
            'SimpleMode' => true,
            'Description' => 'Limit Inodes, 0 for unlimited'
        ),
        'Nofile' => array(
            'Type' => 'text',
            'Default' => '100',
            'SimpleMode' => true,
            'Description' => 'Limit number of Open Files for account'
        ),
        'Nproc' => array(
            'Type' => 'text',
            'Default' => '40',
            'SimpleMode' => true,
            'Description' => 'Limit number of Processes for account, don\'t use 0 as it will not allow any processes'
        )
    );
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_CreateAccount(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'package' => $params['configoption1'],
                'domain' => $params['domain'],
                'key' => $params['serveraccesshash'],
                'action' => 'add',
                'username' => $params['username'],
                'user' => $params['username'],
                'pass' => $params['password'],
                'email' => $params['clientsdetails']['email'],
                'inode' => $params['configoption2'],
                'nofile' => $params['configoption3'],
                'nproc' => $params['configoption4'],
                'server_ips' => $params['serverip']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_SuspendAccount(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'susp',
                'user' => $params['username']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_UnsuspendAccount(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'unsp',
                'user' => $params['username']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_TerminateAccount(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'del',
                'user' => $params['username'],
                'email' => $params['clientsdetails']['email']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/account';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_ChangePassword(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'udp',
                'user' => $params['username'],
                'pass' => $params['password']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/changepass';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return mixed|string
 */
function ihoscwp7_ChangePackage(array $params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'udp',
                'user' => $params['username'],
                'package' => $params['configoption1']
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/changepack';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

            $response = json_decode($response, true);
            if ($response['status'] == 'OK') {
                $result = 'success';
            } else {
                $result = $response;
            }

        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $result;
}

/**
 * @param array $params
 * @return string
 */
function ihoscwp7_ClientArea(array $params)
{
    try {

        $code = '<input type="button" value="Control Panel" onClick="window.open(\'https://' . $params['serverhostname'] . ':2083\')" />
        <input type="button" value="Login to Webmail" onClick="window.open(\'https://' . $params['serverhostname'] . ':2031/roundcube\')" />';

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $code;
}

/**
 * @param $params
 * @return string
 */
function ihoscwp7_AdminLink($params)
{
    $code = '<input type="button" value="Admin Control Panel" onClick="window.open(\'https://' . $params['serverhostname'] . ':2031/login/index.php\')" />';

    return $code;
}

/**
 * @param $params
 */
function ihoscwp7_LoginLink($params)
{
    echo "<a href=\"https://" . $params['serverhostname'] . ":2083\" target=\"_blank\" style=\"color:#cc0000\">Control Panel</a>";
}

function ihoscwp7_LoaderFunction($params)
{
    try {

        if ($params['server'] == 1) {

            $postvars = array(
                'key' => $params['serveraccesshash'],
                'action' => 'list'
            );

            $postdata = http_build_query($postvars);

            $url = 'https://' . $params['serverhostname'] . ':' . $params['serverport'] . '/v1/packages';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
            $response = curl_exec($curl);

            logModuleCall(
                'ihoscwp7',
                __FUNCTION__,
                $url . '/?' . $postdata,
                $response
            );

        }

        if (curl_error($curl)) {
            throw new Exception('Unable to connect: ' . curl_errno($curl) . ' - ' . curl_error($curl));
        } elseif (empty($response)) {
            throw new Exception('Empty response');
        }

        curl_close($curl);

        $packages = json_decode($response, true);

        $packageNames = [];
        foreach ($packages['msj'] as $key => $package) {
            $packageNames[$package['id']] = ucfirst($package['package_name']);
        }

        if (is_null($packageNames)) {
            throw new Exception('Invalid response format');
        }

    } catch (Exception $e) {
        logModuleCall(
            'ihoscwp7',
            __FUNCTION__,
            $params,
            $e->getMessage(),
            $e->getTraceAsString()
        );

        return $e->getMessage();
    }

    return $packageNames;
}
