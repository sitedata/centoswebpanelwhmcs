# ihoscwp7
WHMCS Module (Fix) for Centos-WebPanel (CWP) by IHOS
#
Attention ! The module was created fixed for my private use, in case of error I will be happy to fix it as far as possible!


# Tested on version
WHMCS: 7.7.1
#
CWPpro version: 0.9.8.779


# Features:
* Add Account - Working
* Edit Account - Working
* Suspend Account - Working
* Unsuspend Account - Working
* Delete Account - Working
* Change Password - Working
* Edit Packages - Working

# Future Implementations:

* Admin Dashboard Widgets
#
Provide necessary and important information in your interface in the administrator's home page panel.

* Loader Functions
#
An example use case for this is for a Package or Plan name field, where the values need to be fetched via an API from the remote system where the Packages or Plans are defined. Without a loader function, the end user would have to manually enter the package or plan name for the product. With a loader being used, WHMCS will poll the remote API service for a list of possible values when the field is rendered to the end user and allow them to make a choice.

* Client Area Output
#
Product Details Page Output
#
Creating output to display on the same page as the product details in the client area is easy.

* Admin Services Tab
#
Admin Services Tab functions allow definition of extra fields to appear on the product details in the admin area. Used for informational output, or for settings and values stored in custom tables or outside WHMCS.

# Installation: 

* [1]. Extract file ihoscwp7.zip in your WHMCS folder

WHMCS/modules/servers/

As extracted module file path will look like this WHMCS/modules/servers/ihoscwp7/ihoscwp7.php

* [2]. In WHMCS

In Menu goto: Setup -> Products/Services -> Servers

Add New Server and under "Server Details"

Type = IHOS CWP7

Username: root

Password: (your server password)

Access Hash: copy your key from CWP API

* [3]. Generate API Key on the CWP server

Goto Left-Menu -> CWP Settings -> API Manager

Use the following permissions:

-Add, Edit, Suspend, Unsuspend and Delete Accounts

-Edit Packages

-Change Password

# Updates
Changelog
#
 * [INFO] Date: 13/03/2019
 * [INFO] Version Release: 1.0.0
 * [*] Fix/Revised methods CreateAccount, SuspendAccount, UnsuspendAccount, TerminateAccount, ChangePassword, ChangePackage
 * [+] Added detailed success and error logs in all methods
 * [+] Added verification on file for not directly accessing
 * [+] Added exception handling

More info and Download: 
https://bitbucket.org/vinicius_ihos/ihoscwp7

# ! IMPORTANT
# If there is any problem or you have suggestion to add or request please reply here on the forum.
# Please don't send me PM's.
# Any code fixes, improvements, etc. Bitbucket can be used.
# [Support Development and Learning with Small Donation](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M4JFC5XHJ4V6S&source=url)